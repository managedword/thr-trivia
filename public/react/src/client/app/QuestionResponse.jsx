import React, { Component } from 'react';

class QuestionResponse extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            text: ""
        };
    }

    componentDidMount() {
        let answerResponseText = "";
        this.props.responseVal ? answerResponseText = "Correct!" : answerResponseText = "Wrong!";
        this.setState({ text: answerResponseText });
    }

    render() {

    	let classes = [
    		'response',
    		this.props.responseVal ? 'correct' : 'wrong'
    	].join(' ')

        return(
            <div className={ classes }>

            	<div className="badge">
	            	<div className="label">
		            	{ this.state.text }
	            	</div>
            	</div>

            </div>
        );
    }
}

export default QuestionResponse;