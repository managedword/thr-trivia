import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';

class Question extends React.Component {

	constructor (props) {

		super( props )

	}

	onClickAnswer ( event ) {

		const answer_type = event.target.getAttribute('data-type')

		console.log( answer_type )

		this.props.onAnswerSelected( answer_type )

		event.preventDefault()

	}

	render () {

		const question = this.props.question

		const classes = [
			'question',
			this.props.answerSelected ? 'show_answer' : null
		].join(' ')

		let self = this

		return (

			<div className={ classes } key={ question.id }>
				<p className="text">{ question.text }</p>
				<ul className="answers">
					{ question.answers.map( ( answer, index ) =>
						<li key={ index } className={ 'answer ' + answer.type } data-type={ answer.type } onClick={ self.onClickAnswer.bind(this) }>{ answer.text }</li>
					)}
				</ul>
			</div>

		)
	}
}

export default Question;