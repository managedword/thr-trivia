import React, { Component } from 'react';

class Timer extends React.Component {

    constructor(props){

        super(props)

    }

    render() {
        return(
            <div className="timer">
                <span className="value">{ this.props.time < 1000 ? (this.props.time / 100).toFixed(2) : Math.round(this.props.time / 100).toFixed(0) }</span>
                <span className="label">Time Remaining</span>
            </div>
        );
    }
}

export default Timer;