import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';

import Timer from './Timer.jsx';
import Question from './Question.jsx';
import Score from './Score.jsx';
import QuestionResponse from './QuestionResponse.jsx';
import GameOverScreen from './GameOverScreen.jsx';

class Game extends React.Component {

	constructor(props){

		super(props);

		this.state = {
			width: 0,
			height: 0,
			questions: [],
			questionIndex: 0,
			currentQuestion: { answers: [] },
			selectedAnswer: "",
			answerArray: [],
			score: 0,
			answerSelected: false,
			answerResponse: false,
			gameStage: 0,
			timer: 0,
			maxTimer: 6000
		};

		this.lastAnswerTime = 0

		this.onAnswerSelected = this.onAnswerSelected.bind(this);
		this.onTryAgain = this.onTryAgain.bind(this);

		this.loadGame()

	}

	loadGame () {

		let self = this

		// Used the native React fetch as an example
		fetch('https://mptfevents.org/trivia/wp-json/wp/v2/random/')
			.then((response) => response.json())
			.then((responseJson) => {
				this.setState({
					questions: responseJson,
					currentQuestion: self.prepareQuestion( responseJson[0] ),
					gameStage: 1,
					timer: self.state.maxTimer
				}, () => {

			        //call timer function every second using setInterval
			        self.timerInterval = setInterval( self.updateTimer.bind(this), 10 )

				})
			}).catch( (error) => {
				setTimeout( self.loadGame, 1000 )
			})

	}

	onAnswerSelected ( type ) {

		let newScore = this.state.score;
		let answerResponse = false;
		let answerSelected = true;

		// Let's do some game score math
		let answerTime = this.state.timer
		let answerDiff = ( this.lastAnswerTime - answerTime )
		let answerMultiplier = (6000 - answerDiff) / 6000

		this.lastAnswerTime = answerTime

		console.log( answerTime, answerDiff, answerMultiplier )

		//update score if correct answer chosen
		if( type == 'correct' ){
			newScore += parseInt(1000 * answerMultiplier);
			answerResponse = true;
		}

		//update index for questions array
		let newIndex = this.state.questionIndex + 1;
		this.setState({
			answerSelected: answerSelected,
			answerResponse: answerResponse
		});

		// Shuffle up the questions
		let newQuestion = this.prepareQuestion( this.state.questions[newIndex] )

		if ( type == 'correct' ) {

			setTimeout(() => { this.setState({
				score: newScore,
				questionIndex: newIndex,
				currentQuestion: newQuestion,
				answerSelected: false
			})}, 1000);

		} else {

			setTimeout(() => { this.setState({
					score: newScore,
					questionIndex: newIndex,
					currentQuestion: newQuestion,
					answerSelected: false
			})}, 2000);

		}

		event.preventDefault()

	}

	prepareQuestion ( question ) {

		console.log( question.answers[0].text )

		question.answers = _.shuffle( question.answers )

		return question

	}

	updateTimer () {

		let timer = this.state.timer - 1

		this.setState({
			timer: timer
		})

		if ( timer <= 0 ) {

			clearInterval( this.timerInterval )
			this.setState({ gameStage: 2 })
			this.props.onDone( this.state.score )


		}

	}

	onTryAgain ( event ) {

		let self = this

		this.setState({
			gameStage: 0,
			score: 0
		}, self.loadGame )

		event.preventDefault()

	}

	render () {

		let classes = [
			'game',
			'trivia',
			'stage_' + this.state.gameStage
		].join(' ')

		return(
			<div className={ classes }>
				<div className="stage loading">
					<div className="label">Loading</div>
				</div>
				<div className="stage playing">
					<Timer time={ this.state.timer }/>
					<Score score={ this.state.score }/>
					<Question question={ this.state.currentQuestion } answerSelected={ this.state.answerSelected } onAnswerSelected={ this.onAnswerSelected } />
				</div>
				<GameOverScreen score={ this.state.score } onTryAgain={ this.onTryAgain } onDone={ this.onDone } />
				{ this.state.answerSelected ?  <QuestionResponse responseVal={ this.state.answerResponse }/> : null }
			</div>
		);
	}
}

export default Game;