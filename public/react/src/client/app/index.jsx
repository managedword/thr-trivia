import React from 'react'
import {render} from 'react-dom'
import Game from "./Game.jsx"

(function( $ ) {
	'use strict'

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(function () {

		
		var a=document.getElementsByTagName("a");
		for ( var i=0; i<a.length; i++ ) {
			a[i].onclick=function() {
				window.location=this.getAttribute("href");
				return false
			}
		}

		$('#thr-trivia').each( ( index, element ) => {

			function setScore ( score ) {

				let $score = $('.score input')

				if ($score) {

					$score.val( score )

				}
			}
	
			render( <Game onDone={ setScore } />, element )

		} )


	})


})( jQuery );

