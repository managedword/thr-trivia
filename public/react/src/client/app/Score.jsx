import React, { Component } from 'react';

const Score = ({score}) => {
    

   
    return(
        <div className="score">
            <span className="value">{ score }</span>
            <span className="label">Score</span>
        </div>
    );
}

export default Score;