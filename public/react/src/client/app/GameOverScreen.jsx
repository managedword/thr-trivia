import React, { Component } from 'react';

const GameOverScreen = ({score, onTryAgain, onDone}) => {

	return(
		<div className="stage gameover">
			<div className="modal">
				<div className="label">Thanks for Playing!</div>
				<div className="score">{ score }</div>
				<div className="actions">
					<button className="again et_pb_button et_pb_module" onClick={ onTryAgain }>Try Again</button>
				</div>
			</div>
		</div>
	);
}

export default GameOverScreen;