<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://managedword.com
 * @since             1.0.0
 * @package           Thr_Trivia
 *
 * @wordpress-plugin
 * Plugin Name:       THR Trivia
 * Plugin URI:        https://mptfevents.org/trivia/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Christopher Frazier
 * Author URI:        https://managedword.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       thr-trivia
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-thr-trivia-activator.php
 */
function activate_thr_trivia() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-thr-trivia-activator.php';
	Thr_Trivia_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-thr-trivia-deactivator.php
 */
function deactivate_thr_trivia() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-thr-trivia-deactivator.php';
	Thr_Trivia_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_thr_trivia' );
register_deactivation_hook( __FILE__, 'deactivate_thr_trivia' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-thr-trivia.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_thr_trivia() {

	$plugin = new Thr_Trivia();
	$plugin->run();

}
run_thr_trivia();
