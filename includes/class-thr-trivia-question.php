<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://managedword.com
 * @since      1.0.0
 *
 * @package    Thr_Trivia
 * @subpackage Thr_Trivia/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Thr_Trivia
 * @subpackage Thr_Trivia/admin
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Thr_Trivia_Question {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function register_post_type () {

		/**
		 * Post Type: Questions.
		 */
	
		$labels = array(
			"name" => __( "Questions", "" ),
			"singular_name" => __( "Question", "" ),
		);
	
		$args = array(
			"label" => __( "Questions", "" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "question",
			"has_archive" => true,
			"show_in_menu" => true,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => array( "slug" => "question", "with_front" => true ),
			"query_var" => true,
			"menu_position" => 20,
			"menu_icon" => "dashicons-format-status",
			"supports" => array( "title", "custom-fields" ),
		);
	
		register_post_type( "question", $args );
	}

	public function register_rest_fields () {

		remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );

		add_filter( 'rest_pre_serve_request', function( $value ) {
			header( 'Access-Control-Allow-Origin: *' );
			header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
			header( 'Access-Control-Allow-Credentials: true' );
	
			return $value;

		});

		register_rest_route ( 'wp/v2', '/random', array(
			'methods' => 'GET',
			'callback' => array( 'Thr_Trivia_Question', 'get_random' ),
		) );

		register_rest_field ( 'question', 'answer_1', array(
			'get_callback' => array( 'Thr_Trivia_Question', 'get_custom_field' ),
			'update_callback' => null,
			'schema' => null,
		) );

		register_rest_field ( 'question', 'answer_2', array(
			'get_callback' => array( 'Thr_Trivia_Question', 'get_custom_field' ),
			'update_callback' => null,
			'schema' => null,
		) );

		register_rest_field ( 'question', 'answer_3', array(
			'get_callback' => array( 'Thr_Trivia_Question', 'get_custom_field' ),
			'update_callback' => null,
			'schema' => null,
		) );

		register_rest_field ( 'question', 'answer_4', array(
			'get_callback' => array( 'Thr_Trivia_Question', 'get_custom_field' ),
			'update_callback' => null,
			'schema' => null,
		) );

	}

	static function get_random () {
		$data = [];
		$posts = get_posts( array(
			'post_type' => 'question',
			'orderby' => 'rand',
			'posts_per_page' => 200,
		) );
		foreach ( $posts as $post ) {
			$data[] = [
				'id' => $post->ID,
				'text' => $post->post_title,
				'answers' => [
					[ 'type' => 'correct', 'text' => get_post_meta( $post->ID, 'answer_1', true ) ],
					[ 'type' => 'wrong', 'text' => get_post_meta( $post->ID, 'answer_2', true ) ],
					[ 'type' => 'wrong', 'text' => get_post_meta( $post->ID, 'answer_3', true ) ],
					[ 'type' => 'wrong', 'text' => get_post_meta( $post->ID, 'answer_4', true ) ],
				]
			];
		}
		return $data;
	}

	static function get_custom_field ( $object, $field_name, $request ) {

		return get_post_meta( $object['id'], $field_name, true );

	}

}