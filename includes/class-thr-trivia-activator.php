<?php

/**
 * Fired during plugin activation
 *
 * @link       https://managedword.com
 * @since      1.0.0
 *
 * @package    Thr_Trivia
 * @subpackage Thr_Trivia/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Thr_Trivia
 * @subpackage Thr_Trivia/includes
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Thr_Trivia_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
