<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://managedword.com
 * @since      1.0.0
 *
 * @package    Thr_Trivia
 * @subpackage Thr_Trivia/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Thr_Trivia
 * @subpackage Thr_Trivia/includes
 * @author     Christopher Frazier <chris.frazier@managedword.com>
 */
class Thr_Trivia_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
